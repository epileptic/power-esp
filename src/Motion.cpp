#include <Motion.h>

Motion::Motion(int din) {
    Motion::din = din;
    pinMode(din, INPUT);
}

bool Motion::getValue() {
    return digitalRead(din);
}