#include <MyIOT.h>

MyIOT::MyIOT(WiFiClient &_wifiClient, CMEM &cmem) : wifiClient(_wifiClient), cmem(cmem)
{
    firstLoop = true;
}

void MyIOT::reinitializePubSub()
{
    // load deviceName
    deviceName = cmem.value(Const::CONFIG_DEVICE_NAME_KEY, Const::DEVICE_NAME);

    // disconnect psc if not nullptr
    if (psc)
        psc->disconnect();

    // instatiate psc
    psc = new PubSubClient(wifiClient);

    //psc->setServer(cmem.value(Const::CONFIG_SERVER_IP_KEY, Const::MQTT_SERVER_IP).c_str(), cmem.value(Const::CONFIG_SERVER_PORT_KEY, Const::MQTT_SERVER_PORT));
    String serverIp = cmem.value(Const::CONFIG_SERVER_IP_KEY, Const::MQTT_SERVER_IP);
    int serverPort = cmem.value(Const::CONFIG_SERVER_PORT_KEY, Const::MQTT_SERVER_PORT);
    psc->setServer(serverIp.c_str(), serverPort);

    // set psc callback
    using std::placeholders::_1;
    using std::placeholders::_2;
    using std::placeholders::_3;
    psc->setCallback(std::bind(&MyIOT::callback, this, _1, _2, _3));

    // connect
    psc->connect(deviceName.c_str());
}

void MyIOT::callback(char *topic, byte *payload, unsigned int length)
{
    String topicStr = String(topic);
    String payloadStr;

    for (unsigned int i = 0; i < length; i++)
    {
        payloadStr += (char)payload[i];
    }

    // output debug message
    Serial.println("Message received! --> topic:" + topicStr + "; payload:" + payloadStr);

    // parse payload
    StaticJsonDocument<128> doc;
    DeserializationError err = deserializeJson(doc, payloadStr.c_str());

    // handle message
    if (topicStr == mqttRootTopic + Const::MQTT_STATE_GET_APPENDIX)
    {
        getState();
    }
    else if (topicStr == mqttRootTopic + Const::MQTT_SENSORS_GET_APPENDIX)
    {
        getSensors();
    }
    else if (topicStr == mqttRootTopic + Const::MQTT_COMMAND_GET_APPENDIX)
    {
        getCommand();
    }
    else if (topicStr == mqttRootTopic + Const::MQTT_COMMAND_EXECUTE_APPENDIX)
    {
        if (doc.is<JsonObject>() && err == OK)
            executeCommand(doc.as<JsonObject>());
    }
    else if (topicStr == mqttRootTopic + Const::MQTT_CONFIG_GET_APPENDIX)
    {
        getConfig();
    }
    else if (topicStr == mqttRootTopic + Const::MQTT_CONFIG_SET_APPENDIX)
    {
        if (doc.is<JsonObject>() && err == OK)
            setConfig(doc.as<JsonObject>());
    }
    else if (topicStr.startsWith(mqttRootTopic + Const::MQTT_EXTEND_ROOT_APPENDIX))
    {
        extend(topicStr, payloadStr);
    }
}

void MyIOT::getState()
{
    DynamicJsonDocument doc(1024);
    //doc["firmware"].add(Const::FIRMWARE_NAME);
    //doc["firmware"].add(Const::FIRMWARE_VERSION);
    //doc["name"] = deviceName;
    doc["ip"] = WiFi.localIP().toString();
    //doc["hostname"] = wifi_station_get_hostname;
    doc["rstc"] = ESP.getResetReason();

    String docStr;
    serializeJson(doc, docStr);

    if (psc)
        psc->publish((mqttRootTopic + Const::MQTT_STATE_POST_APPENDIX).c_str(), docStr.c_str());
}

void MyIOT::getSensors()
{
}

void MyIOT::extend(String topic, String payload)
{
    Serial.println("extend");
}

void MyIOT::getCommand()
{
    DynamicJsonDocument doc(1024);
    String docStr;

    doc.add(Const::CONFIG_CHANGE_DELAY_KEY);
    doc.add(Const::CONFIG_CHANGE_RESOLUTION_KEY);
    doc.add(Const::CONFIG_CHASE_ONE_COLOR_B_KEY);
    doc.add(Const::CONFIG_CHASE_ONE_COLOR_G_KEY);
    doc.add(Const::CONFIG_CHASE_ONE_COLOR_R_KEY);
    docStr = String();
    serializeJson(doc, docStr);
    psc->publish((mqttRootTopic + Const::MQTT_COMMAND_POST_APPENDIX).c_str(), docStr.c_str());

    doc = DynamicJsonDocument(1024);
    doc.add(Const::CONFIG_CHASE_SPEED_KEY);
    doc.add(Const::CONFIG_CHASE_TWO_COLOR_B_KEY);
    doc.add(Const::CONFIG_CHASE_TWO_COLOR_G_KEY);
    doc.add(Const::CONFIG_CHASE_TWO_COLOR_R_KEY);
    doc.add(Const::CONFIG_COLOR_B_KEY);
    docStr = String();
    serializeJson(doc, docStr);
    psc->publish((mqttRootTopic + Const::MQTT_COMMAND_POST_APPENDIX).c_str(), docStr.c_str());

    doc = DynamicJsonDocument(1024);
    doc.add(Const::CONFIG_COLOR_G_KEY);
    doc.add(Const::CONFIG_COLOR_R_KEY);
    doc.add(Const::CONFIG_CURRENT_MODE_KEY);
    doc.add(Const::CONFIG_DEVICE_NAME_KEY);
    doc.add(Const::CONFIG_FADE_SPEED_KEY);
    docStr = String();
    serializeJson(doc, docStr);
    psc->publish((mqttRootTopic + Const::MQTT_COMMAND_POST_APPENDIX).c_str(), docStr.c_str());

    doc = DynamicJsonDocument(1024);
    doc.add(Const::CONFIG_FILE_LOCATION);
    doc.add(Const::CONFIG_MOTION_PIN_KEY);
    doc.add(Const::CONFIG_RAINBOW_SPEED_KEY);
    doc.add(Const::CONFIG_SERVER_IP_KEY);
    doc.add(Const::CONFIG_SERVER_PORT_KEY);
    docStr = String();
    serializeJson(doc, docStr);
    psc->publish((mqttRootTopic + Const::MQTT_COMMAND_POST_APPENDIX).c_str(), docStr.c_str());

    doc = DynamicJsonDocument(1024);
    doc.add(Const::CONFIG_STRIP_AMOUNT_KEY);
    doc.add(Const::CONFIG_STRIP_PIN_KEY);
    doc.add(Const::CONFIG_SUSPEND_KEY);
    doc.add(Const::CONFIG_TEMPERATURE_PIN_KEY);
    doc.add(Const::CONFIG_UPDATER_PASSWORD_KEY);
    docStr = String();
    serializeJson(doc, docStr);
    psc->publish((mqttRootTopic + Const::MQTT_COMMAND_POST_APPENDIX).c_str(), docStr.c_str());

    doc = DynamicJsonDocument(1024);
    doc.add(Const::CONFIG_UPDATER_PATH_KEY);
    doc.add(Const::CONFIG_UPDATER_USERNAME_KEY);
    doc.add(Const::CONFIG_WIFI_PWD_KEY);
    doc.add(Const::CONFIG_WIFI_SSID_KEY);
    docStr = String();
    serializeJson(doc, docStr);
    psc->publish((mqttRootTopic + Const::MQTT_COMMAND_POST_APPENDIX).c_str(), docStr.c_str());
}

void MyIOT::executeCommand(JsonObject jdoc)
{
    for (const auto &i : jdoc)
    {
        if (!cmem.getConfig()[i.key()].isNull())
        {
            cmem.getConfig()[i.key()] = i.value();
        }
    }

    if (jdoc[Const::COMMAND_FASTCMD].is<JsonArray>())
    {
        for (const auto &i : jdoc[Const::COMMAND_FASTCMD].as<JsonArray>())
        {
            if (i.as<String>())
            {
                String cmd = i.as<String>();
                if (cmd == "spiffs_format")
                    SPIFFS.format();
                else if (cmd == "restart")
                    ESP.restart();
                else if (cmd == "reset")
                    ESP.reset();
                else if (cmd == "tte")
                    Serial.println(1 / 0);
            }
        }
    }
}

void MyIOT::getConfig()
{
    Serial.println("getConfig");
}

void MyIOT::setConfig(JsonObject jdoc)
{
    for (const auto &i : jdoc)
        if (cmem.getPmem().getConfig().containsKey(i.key()))
            cmem.getPmem().getConfig()[i.key()] = i.value();
        else
            serializeJson(cmem.getPmem().getConfig(), Serial);
    cmem.getPmem().flash();
}

PubSubClient *MyIOT::getPsc()
{
    return psc;
}

void MyIOT::subscribe()
{
    mqttRootTopic = Const::MQTT_ROOT_SUFFIX + cmem.value(Const::CONFIG_DEVICE_NAME_KEY, Const::DEVICE_NAME) + "/";

    psc->subscribe((mqttRootTopic + Const::MQTT_STATE_GET_APPENDIX).c_str());
    psc->subscribe((mqttRootTopic + Const::MQTT_SENSORS_GET_APPENDIX).c_str());

    psc->subscribe((mqttRootTopic + Const::MQTT_COMMAND_GET_APPENDIX).c_str());
    psc->subscribe((mqttRootTopic + Const::MQTT_COMMAND_EXECUTE_APPENDIX).c_str());

    psc->subscribe((mqttRootTopic + Const::MQTT_CONFIG_GET_APPENDIX).c_str());
    psc->subscribe((mqttRootTopic + Const::MQTT_CONFIG_SET_APPENDIX).c_str());

    psc->subscribe((mqttRootTopic + Const::MQTT_EXTEND_ROOT_APPENDIX).c_str());
}

void MyIOT::loop()
{
    wifiConnected = (WiFi.status() == WL_CONNECTED);
    mqttConnected = wifiConnected && psc && psc->connected();

    if (wifiConnected)
    {
        if (!lastWiFiConnected)
            Serial.println("WiFi established!");
        if (!mqttConnected)
        {
            if (!psc || mqttReconnectionAttempt >= 10)
            {
                reinitializePubSub();
                Serial.println("MQTT Reinstantiated!");

                mqttReconnectionAttempt = 0;
                delay(100);
            }
            else
            {
                Serial.print("Recovering MQTT connection... ");
                Serial.println(mqttReconnectionAttempt);

                mqttReconnectionAttempt++;
                delay(500);
            }
        }
        else if (!lastMqttConnected)
        {
            Serial.println("MQTT established!");
            subscribe();
        }
    }
    else if (lastWiFiConnected)
    {
        Serial.println("WiFi lost!");
    }
    lastWiFiConnected = wifiConnected;
    lastMqttConnected = mqttConnected;

    online = mqttConnected && wifiConnected && psc;

    // loop psc
    if (psc)
        psc->loop();

    if (firstLoop && online)
    {
        getState();
        firstLoop = false;
    }

    // motion push
    if (motionSensor && online)
    {
        currentMotion = motionSensor->getValue();
        if (currentMotion != lastMotion)
        {
            psc->publish((mqttRootTopic + Const::MQTT_SENSORS_POST_APPENDIX + Const::MOTION_SENSOR_APPENDIX).c_str(), String(currentMotion).c_str());
            lastMotion = currentMotion;
        }
    }

    // temperature push
    if (temperatureSensor && online)
    {
        temperatureSensor->requestTemperatures();
        currentTemp = temperatureSensor->getTempCByIndex(0);
        if (currentTemp != lastTemp)
        {
            psc->publish((mqttRootTopic + Const::MQTT_SENSORS_POST_APPENDIX + Const::TEMPERATURE_SENSOR_APPENDIX).c_str(), String(currentTemp).c_str());
            lastTemp = currentTemp;
        }
    }

    int changeSpeed = cmem.value(Const::CONFIG_CHANGE_DELAY_KEY, Const::CHANGE_DELAY);
    int changeResolution = cmem.value(Const::CONFIG_CHANGE_RESOLUTION_KEY, Const::CHANGE_RESOLUTION);
    String currentMode = cmem.value(Const::CONFIG_CURRENT_MODE_KEY, Const::CURRENT_MODE);

    // strip handling
    if (strip)
    {
        // check if suspended
        if (cmem.value(Const::CONFIG_SUSPEND_KEY, Const::SUSPENDED))
        {
            // fadoff if was not suspended before
            if (!lastSuspended)
            {
                if (fx)
                    fx->fadeOff(changeSpeed, changeResolution);
                oldColor[0] = 0;
                oldColor[1] = 0;
                oldColor[2] = 0;
            }
        }
        else
        {
            // Check global brightness
            if (cmem.value(Const::CONFIG_GLOBAL_BRIGHTNESS_KEY, Const::GLOBAL_BRIGHTNESS) != lastGlobalBrightness)
            {
                int globalBrightness = cmem.value(Const::CONFIG_GLOBAL_BRIGHTNESS_KEY, Const::GLOBAL_BRIGHTNESS);
                strip->setBrightness(globalBrightness);
                lastGlobalBrightness = globalBrightness;
            }

            // Check for mode change
            if (cmem.value(Const::CONFIG_CURRENT_MODE_KEY, Const::CURRENT_MODE) != lastMode)
            {
                if (fx)
                    fx->fadeOff(changeSpeed, changeResolution);
                oldColor[0] = 0;
                oldColor[1] = 0;
                oldColor[2] = 0;
            }

            if (currentMode == "color")
            {
                if (fx)
                {
                    int color[3] = {cmem.value(Const::CONFIG_COLOR_R_KEY, Const::COLOR_R), cmem.value(Const::CONFIG_COLOR_G_KEY, Const::COLOR_G), cmem.value(Const::CONFIG_COLOR_B_KEY, Const::COLOR_B)};
                    if (color[0] != oldColor[0] || color[1] != oldColor[1] || color[2] != oldColor[2] || lastMode != currentMode)
                    {
                        fx->change(oldColor, color, changeResolution, changeSpeed);
                        for (unsigned int i = 0; i < 3; i++)
                            oldColor[i] = color[i];
                    }
                    else
                    {
                        strip->show();
                        delay(500);
                    }
                }
            }
            else if (currentMode == "fade")
            {
                if (fx)
                    fx->fade(changeSpeed);
            }
            else if (currentMode == "rainbow")
            {
                if (fx)
                    fx->rainbow(cmem.value(Const::CONFIG_RAINBOW_SPEED_KEY, Const::RAINBOW_SPEED));
            }
            else if (currentMode == "chase")
            {
                int chaseOne[3] = {cmem.value(Const::CONFIG_CHASE_ONE_COLOR_R_KEY, Const::CHASE_ONE_R), cmem.value(Const::CONFIG_CHASE_ONE_COLOR_G_KEY, Const::CHASE_ONE_G), cmem.value(Const::CONFIG_CHASE_ONE_COLOR_B_KEY, Const::CHASE_ONE_B)};
                int chaseTwo[3] = {cmem.value(Const::CONFIG_CHASE_TWO_COLOR_R_KEY, Const::CHASE_TWO_R), cmem.value(Const::CONFIG_CHASE_TWO_COLOR_G_KEY, Const::CHASE_TWO_G), cmem.value(Const::CONFIG_CHASE_TWO_COLOR_B_KEY, Const::CHASE_TWO_B)};
                if (fx)
                    fx->chase(cmem.value(Const::CONFIG_CHASE_SPEED_KEY, Const::CHASE_SPEED), chaseOne, chaseTwo, cmem.value(Const::CONFIG_CHASE_CLUSTER_SIZE_KEY, Const::CHASE_CLUSTER_SIZE), cmem.value(Const::CONFIG_CHASE_CLUSTER_DISTANCE_KEY, Const::CHASE_CLUSTER_DISTANCE));
            }
            else if (currentMode == "off")
            {
                strip->clear();
                strip->show();
                delay(500);
            }
            else
            {
                // update current mode
                currentMode = lastMode;
            }

            // update last mode
            lastMode = currentMode;
        }

        // update last suspended
        lastSuspended = cmem.value(Const::CONFIG_SUSPEND_KEY, Const::SUSPENDED);
    }
}

void MyIOT::setStrip(Adafruit_NeoPixel *strip)
{
    MyIOT::strip = strip;
    strip->begin();
    MyIOT::fx = new Effects(strip);
}

Adafruit_NeoPixel *MyIOT::getStrip()
{
    return strip;
}

Effects *MyIOT::getEffects()
{
    return fx;
}

void MyIOT::setMotion(Motion *motion)
{
    motionSensor = motion;
}

Motion *MyIOT::getMotion()
{
    return motionSensor;
}

void MyIOT::setTemperature(DallasTemperature *temperature)
{
    temperatureSensor = temperature;
}

DallasTemperature *MyIOT::getTemperature()
{
    return temperatureSensor;
}