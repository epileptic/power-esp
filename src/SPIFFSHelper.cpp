#include <SPIFFSHelper.h>

void SPIFFSHelper::printFile(File subject)
{
    // printout all bytes as chars
    for (unsigned int i = 0; i < subject.size(); i++)
        Serial.print((char)subject.read());
    Serial.println();
}