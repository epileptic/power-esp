#include <Arduino.h>
#include <OneWire.h>

class Temperature
{
private:
    int din;

public:
    Temperature(int din);
    int getValue();
};