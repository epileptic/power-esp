#pragma once
#include <Arduino.h>

class Const
{
public:
    /* Arduino json */
    static const int AJ_SPIFFS_HELPER_LOAD_CONFIG_SIZE;

    /* Firmware info */
    static const String FIRMWARE_NAME;
    static const String FIRMWARE_STATE;
    static const float FIRMWARE_VERSION;

    /* Device signature */
    static const String DEVICE_NAME;

    /* WiFi config */
    static const String WIFI_SSID;
    static const String WIFI_PWD;

    /* Web updater config */
    static const String UPDATER_PATH;
    static const String UPDATER_USERNAME;
    static const String UPDATER_PASSWORD;

    /* MQTT config */
    static const String MQTT_SERVER_IP;
    static const int MQTT_SERVER_PORT;

    /* Mode config */
    static const boolean SUSPENDED;
    static const String CURRENT_MODE;
    static const int GLOBAL_BRIGHTNESS;

    /* Effect change config */
    static const int CHANGE_RESOLUTION;
    static const int CHANGE_DELAY;

    /* Effect speed config */
    static const int RAINBOW_SPEED;
    static const int FADE_SPEED;
    static const int CHASE_SPEED;

    /* "color" effect config */
    static const int COLOR_R;
    static const int COLOR_G;
    static const int COLOR_B;

    /* "chase" effect config */
    static const int CHASE_ONE_R;
    static const int CHASE_ONE_G;
    static const int CHASE_ONE_B;

    static const int CHASE_TWO_R;
    static const int CHASE_TWO_G;
    static const int CHASE_TWO_B;

    static const int CHASE_CLUSTER_SIZE;
    static const int CHASE_CLUSTER_DISTANCE;

    /* Config file config */
    static const String CONFIG_FILE_LOCATION;

    /* CONFIG-KEY device signature*/
    static const String CONFIG_DEVICE_NAME_KEY;

    /* CONFIG-KEY WiFi config*/
    static const String CONFIG_WIFI_SSID_KEY;
    static const String CONFIG_WIFI_PWD_KEY;

    /* CONFIG-KEY updater config */
    static const String CONFIG_UPDATER_PATH_KEY;
    static const String CONFIG_UPDATER_USERNAME_KEY;
    static const String CONFIG_UPDATER_PASSWORD_KEY;

    /* CONFIG-KEY MQTT config */
    static const String CONFIG_SERVER_IP_KEY;
    static const String CONFIG_SERVER_PORT_KEY;

    /* CONFIG-KEY mode config*/
    static const String CONFIG_SUSPEND_KEY;
    static const String CONFIG_CURRENT_MODE_KEY;
    static const String CONFIG_GLOBAL_BRIGHTNESS_KEY;

    /* CONFIG-KEY effect change config*/
    static const String CONFIG_CHANGE_RESOLUTION_KEY;
    static const String CONFIG_CHANGE_DELAY_KEY;

    /* CONFIG-KEY effect speed config */
    static const String CONFIG_RAINBOW_SPEED_KEY;
    static const String CONFIG_FADE_SPEED_KEY;
    static const String CONFIG_CHASE_SPEED_KEY;

    /* CONFIG-KEY "color" effect config */
    static const String CONFIG_COLOR_R_KEY;
    static const String CONFIG_COLOR_G_KEY;
    static const String CONFIG_COLOR_B_KEY;

    /* CONIFG-KEY "chase" effect config */
    static const String CONFIG_CHASE_ONE_COLOR_R_KEY;
    static const String CONFIG_CHASE_ONE_COLOR_G_KEY;
    static const String CONFIG_CHASE_ONE_COLOR_B_KEY;

    static const String CONFIG_CHASE_TWO_COLOR_R_KEY;
    static const String CONFIG_CHASE_TWO_COLOR_G_KEY;
    static const String CONFIG_CHASE_TWO_COLOR_B_KEY;

    static const String CONFIG_CHASE_CLUSTER_SIZE_KEY;
    static const String CONFIG_CHASE_CLUSTER_DISTANCE_KEY;

    /* CONFIG-KEY strip config*/
    static const String CONFIG_STRIP_PIN_KEY;
    static const String CONFIG_STRIP_AMOUNT_KEY;

    /* CONFIG-KEY sensor config*/
    static const String CONFIG_TEMPERATURE_PIN_KEY;
    static const String CONFIG_MOTION_PIN_KEY;

    /* CONFIG-KEY command config */
    static const String COMMAND_FASTCMD;

    /* CONFIG-KEY mqtt topic config*/
    static const String MQTT_ROOT_SUFFIX;

    static const String MQTT_STATE_GET_APPENDIX;
    static const String MQTT_STATE_POST_APPENDIX;

    static const String MQTT_SENSORS_GET_APPENDIX;
    static const String MQTT_SENSORS_POST_APPENDIX;

    static const String MQTT_COMMAND_GET_APPENDIX;
    static const String MQTT_COMMAND_POST_APPENDIX;
    static const String MQTT_COMMAND_EXECUTE_APPENDIX;

    static const String MQTT_CONFIG_GET_APPENDIX;
    static const String MQTT_CONFIG_SET_APPENDIX;

    static const String MQTT_EXTEND_ROOT_APPENDIX;

    static const String MOTION_SENSOR_APPENDIX;
    static const String TEMPERATURE_SENSOR_APPENDIX;
};