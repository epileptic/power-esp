#pragma once
#include <Arduino.h>
#include <ArduinoJson.h>
#include <Const.h>
#include <FS.h>

class PMEM
{
private:
    String configFilePath;
    JsonDocument *config;
    boolean changedSinceFlash;

    void change();

public:
    PMEM(String configFilePath);

    template <typename T>
    T inform(String key, T defaultValue)
    {
        if (config->getMember(key).is<T>())
        {
            return config->getMember(key);
        }
        else
        {
            config->getOrAddMember(key).set(defaultValue);
            change();
            return defaultValue;
        }
    }

    template <typename T>
    T value(String key, T defaultValue)
    {
        if (config->getMember(key).is<T>)
        {
            return config->getMember(key);
        }
        else
        {
            config->getOrAddMember(key).set(defaultValue);
            change();
            return defaultValue;
        }
    }

    template <typename T>
    void set(String key, T value)
    {
        config->getOrAddMember(key).set(value);
        change();
    }

    JsonDocument &getConfig();

    void flash();
};
