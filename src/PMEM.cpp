#include <PMEM.h>

PMEM::PMEM(String configFilePath) : configFilePath(configFilePath)
{
    SPIFFS.begin();
    config = new DynamicJsonDocument(Const::AJ_SPIFFS_HELPER_LOAD_CONFIG_SIZE);
    File configFile = SPIFFS.open(configFilePath, "r");
    deserializeJson(*config, configFile);
}

void PMEM::change()
{
    changedSinceFlash = true;
}

JsonDocument &PMEM::getConfig()
{
    change();
    return *config;
}

void PMEM::flash()
{
    if (changedSinceFlash)
    {
        changedSinceFlash = false;
        File cf = SPIFFS.open(configFilePath, "w");
        serializeJson(*config, cf);
        cf.close();
    }
}