#include <Arduino.h>

class Motion
{
private:
    int din;

public:
    Motion(int din);
    bool getValue();
};