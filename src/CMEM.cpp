#include <CMEM.h>

CMEM::CMEM(JsonDocument &config, PMEM &pmem) : config(config), pmem(pmem)
{
}

void CMEM::dumpToPMEM()
{
    for (const auto &i : config.as<JsonObject>())
        if (!pmem.getConfig()[i.key()].isNull())
            pmem.getConfig()[i.key()] = i.value();
}

JsonDocument &CMEM::getConfig()
{
    return config;
}

PMEM &CMEM::getPmem()
{
    return pmem;
}