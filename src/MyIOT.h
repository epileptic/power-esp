#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <ArduinoJson.h>
#include <Const.h>
#include <CMEM.h>
#include <DallasTemperature.h>
#include <Effects.h>
#include <ESP8266WiFi.h>
#include <Motion.h>
#include <OneWire.h>
#include <PubSubClient.h>

class MyIOT
{
private:
    /* private fields */
    // device signature
    String deviceName;
    String mqttRootTopic;

    // communication objects
    PubSubClient *psc;
    WiFiClient &wifiClient;

    // online check
    boolean online = false;

    // mqtt connection status
    boolean mqttConnected = false;
    boolean lastMqttConnected = false;
    int mqttReconnectionAttempt = 0;

    // wifi connection status
    boolean wifiConnected = false;
    boolean lastWiFiConnected = false;

    // config

    // led effect
    Adafruit_NeoPixel *strip;
    Effects *fx;

    // motion sensor status
    bool currentMotion;
    bool lastMotion;
    Motion *motionSensor;

    // temperature sensor status
    float currentTemp;
    float lastTemp;
    DallasTemperature *temperatureSensor;

    // suspend status
    bool lastSuspended = false;

    // mode status
    String lastMode;

    // global brightness status
    double lastGlobalBrightness;

    // "color" mode config
    int oldColor[3] = {0, 0, 0};

    // logic status
    bool firstLoop;

    // new config system
    CMEM &cmem;

    /* private methods */
    void callback(char *topic, byte *payload, unsigned int length);
    void reinitializePubSub();

public:
    // constructor
    MyIOT(WiFiClient &wifiClient, CMEM &cmem);

    // special functions
    void getState();
    void getSensors();
    void extend(String topic, String payload);

    // commands
    void getCommand();
    void executeCommand(JsonObject jdoc);

    // configuration
    void getConfig();
    void setConfig(JsonObject jdoc);

    // psc config
    PubSubClient *getPsc();

    // subscribe to topics
    void subscribe();
    void loop();

    // strip
    void setStrip(Adafruit_NeoPixel *strip);
    Adafruit_NeoPixel *getStrip();
    Effects *getEffects();

    // motion
    void setMotion(Motion *motion);
    Motion *getMotion();

    // temperature
    void setTemperature(DallasTemperature *temperature);
    DallasTemperature *getTemperature();
};