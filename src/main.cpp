#include <Arduino.h>
#include <Const.h>
#include <CMEM.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <MyIOT.h>
#include <PMEM.h>

// wifi setup
WiFiClient wifiClient;

// new configuration method
StaticJsonDocument<2048> memdoc;
PMEM pmem("/config.json");
CMEM cmem(memdoc, pmem);

// config setup
MyIOT iot(wifiClient, cmem);

// http server setup
ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;

void info()
{
  String name = "Device Name:" + cmem.value(Const::CONFIG_DEVICE_NAME_KEY, Const::DEVICE_NAME);
  String firmware = "Firmware   :" + Const::FIRMWARE_NAME + "_V" + Const::FIRMWARE_VERSION + "-" + Const::FIRMWARE_STATE;
  String configStr;
  serializeJson(cmem.getConfig(), configStr);
  httpServer.send(200, "text/plain", name + "\n" + firmware + "\n" + configStr);
}

void setup()
{
  // serial
  Serial.begin(9600);
  Serial.println();

  // increase software watchdog timout
  ESP.wdtEnable(5000);

  // output some board info
  Serial.println("-->> Board info --------");
  Serial.println("ESP.getFlashChipSize():" + String(ESP.getFlashChipSize()));
  Serial.println("ESP.getFlashChipRealSize():" + String(ESP.getFlashChipRealSize()));
  Serial.println("ESP.getFullVersion():" + String(ESP.getFullVersion()));
  Serial.println("ESP.getResetInfo():" + String(ESP.getResetInfo()));
  Serial.println("ESP.getResetReason():" + ESP.getResetReason());
  Serial.println("Current power-esp config:");
  serializeJson(cmem.getConfig(), Serial);
  Serial.println();

  // updater
  httpUpdater.setup(&httpServer, cmem.value(Const::CONFIG_UPDATER_PATH_KEY, Const::UPDATER_PATH), cmem.value(Const::CONFIG_UPDATER_USERNAME_KEY, Const::UPDATER_USERNAME), cmem.value(Const::CONFIG_UPDATER_PASSWORD_KEY, Const::UPDATER_PASSWORD));
  httpServer.begin();
  httpServer.on("/", info);

  // wifi
  WiFi.mode(WIFI_STA);
  String ssid = cmem.value(Const::CONFIG_WIFI_SSID_KEY, Const::WIFI_SSID);
  String pwd = cmem.value(Const::CONFIG_WIFI_PWD_KEY, Const::WIFI_PWD);
  WiFi.begin(ssid, pwd);

  // get strip config
  int stripPin = cmem.value(Const::CONFIG_STRIP_PIN_KEY, -1);
  int stripAmount = cmem.value(Const::CONFIG_STRIP_AMOUNT_KEY, 16);

  // get sensor config
  int tempPin = cmem.value(Const::CONFIG_TEMPERATURE_PIN_KEY, -1);
  int motionPin = cmem.value(Const::CONFIG_MOTION_PIN_KEY, -1);

  // register strip if present
  if (stripPin != -1)
    iot.setStrip(new Adafruit_NeoPixel(stripAmount, stripPin, NEO_GRB + NEO_KHZ400));

  // register temperature sensor if present
  if (tempPin != -1)
    iot.setTemperature(new DallasTemperature(new OneWire(tempPin)));

  // register temperature sensor if present
  if (motionPin != -1)
    iot.setMotion(new Motion(motionPin));
}

void loop()
{
  // handle updater and help page
  httpServer.handleClient();

  // loop iot
  iot.loop();

  // feed watchdog
  ESP.wdtFeed();
}