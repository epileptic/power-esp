#include <Effects.h>

Effects::Effects(Adafruit_NeoPixel *strip)
{
    Effects::strip = strip;
    strip->clear();
    strip->show();
}

void Effects::change(int oldColor[3], int newColor[3], float steps, int wait)
{
    // calculate color deltas
    float deltaR = (newColor[0] - oldColor[0]) / steps;
    float deltaG = (newColor[1] - oldColor[1]) / steps;
    float deltaB = (newColor[2] - oldColor[2]) / steps;

    // fade
    for (int i = 0; i < steps; i++)
    {
        strip->fill(strip->Color(oldColor[0] + (deltaR * i), oldColor[1] + (deltaG * i), oldColor[2] + (deltaB * i)), 0, strip->numPixels());

        // show and wait
        strip->show();
        delay(wait);
    }

    // set new color and update
    strip->fill(strip->Color(newColor[0], newColor[1], newColor[2]), 0, strip->numPixels());
    strip->show();
}

void Effects::rainbow(int wait)
{
    // cycle hue
    if (rainbowHue < 5 * 65536)
    {
        rainbowHue += 256;
        for (int i = 0; i < strip->numPixels(); i++)
        {
            // set leds
            int pixelHue = rainbowHue + (i * 65536L / strip->numPixels());
            strip->setPixelColor(i, strip->gamma32(strip->ColorHSV(pixelHue)));
        }

        // show and wait
        strip->show();
        delay(wait);
    }
    else
    {
        rainbowHue = 0;
    }
}

void Effects::fade(int wait)
{
    if (fadeHue < 5 * 65536)
    {
        strip->fill(strip->ColorHSV(fadeHue), 0, strip->numPixels());
        strip->show();
        delay(wait);
        fadeHue += 256;
    }
    else
    {
        fadeHue = 0;
        strip->clear();
    }
}

void Effects::chase(int wait, int chaseOne[3], int chaseTwo[3], int clusterSize, int clusterDistance)
{
    strip->clear();
    if (chaseMirror)
        chaseFill(chaseOne, chaseTwo, clusterSize, clusterDistance);
    else
        chaseFill(chaseTwo, chaseOne, clusterSize, clusterDistance);
    chaseMirror = !chaseMirror;
    strip->show();

    delay(wait);
}

void Effects::chaseFill(int chaseOne[3], int chaseTwo[3], int clusterSize, int clusterDistance)
{
    int fieldSize = clusterSize * 2 + clusterDistance * 2;
    int submod = 0;
    for (int i = 0; i < strip->numPixels(); i++)
    {
        submod = i % (fieldSize / 2);
        if (submod < clusterSize)
        {
            if ((i / (fieldSize / 2)) % 2 == 0)
                strip->setPixelColor(i, strip->Color(chaseOne[0], chaseOne[1], chaseOne[2]));
            else
                strip->setPixelColor(i, strip->Color(chaseTwo[0], chaseTwo[1], chaseTwo[2]));
        }
    }
}

void Effects::fadeOff(int wait, float steps)
{
    // go through all steps
    for (float i = 0; i < steps; i++)
    {
        // set color for every pixel
        for (int j = 0; j < strip->numPixels(); j++)
        {
            float originR = uint8_t(strip->getPixelColor(j) >> 16);
            float originG = uint8_t(strip->getPixelColor(j) >> 8);
            float originB = uint8_t(strip->getPixelColor(j));

            int r = round(originR - (i * (originR / steps)));
            int g = round(originG - (i * (originG / steps)));
            int b = round(originB - (i * (originB / steps)));

            strip->setPixelColor(j, strip->Color(r, g, b));
        }

        // show and wait
        strip->show();
        delay(wait);
    }

    // clear everything in case of floatingpoint issues
    strip->clear();
    strip->show();
}