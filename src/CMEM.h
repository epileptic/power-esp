#pragma once
#include <Arduino.h>
#include <ArduinoJson.h>
#include <PMEM.h>

class CMEM
{
private:
    JsonDocument &config;
    PMEM &pmem;

public:
    CMEM(JsonDocument &config, PMEM &pmem);

    template <typename T>
    T value(String key, T defaultValue)
    {
        if (!config[key].is<T>())
            config[key] = pmem.inform(key, defaultValue);
        return config[key];
    }

    template <typename T>
    boolean set(String key, T value)
    {
        if (config[key].is<T>())
        {
            config[key] = value;
            return true;
        }
        else
        {
            return false;
        }
    }

    void dumpToPMEM();

    JsonDocument &getConfig();

    PMEM &getPmem();
};