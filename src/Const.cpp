#include <Const.h>

/* Arduino json */

// RENAME
const int Const::AJ_SPIFFS_HELPER_LOAD_CONFIG_SIZE = 1024;

/* Firmware info */
const String Const::FIRMWARE_NAME = "SC_INDIV";
const String Const::FIRMWARE_STATE = "vanilla_beta";
const float Const::FIRMWARE_VERSION = 3.01;

/* Device signature */
const String Const::DEVICE_NAME = "new-device";

/* WiFi config */
const String Const::WIFI_SSID = "";
const String Const::WIFI_PWD = "";

/* Web updater config */
const String Const::UPDATER_PATH = "/update";
const String Const::UPDATER_USERNAME = "admin";
const String Const::UPDATER_PASSWORD = "Admin";

/* MQTT config */
const String Const::MQTT_SERVER_IP = "";
const int Const::MQTT_SERVER_PORT = 1883;

/* Mode config */
const boolean Const::SUSPENDED = false;
const String Const::CURRENT_MODE = "off";
const int Const::GLOBAL_BRIGHTNESS = 100;

/* Effect change config */
const int Const::CHANGE_RESOLUTION = 100;
const int Const::CHANGE_DELAY = 10;

/* Effect speed config */
const int Const::RAINBOW_SPEED = 25;
const int Const::FADE_SPEED = 25;
const int Const::CHASE_SPEED = 750;

/* "color" effect config */
const int Const::COLOR_R = 0;
const int Const::COLOR_G = 0;
const int Const::COLOR_B = 0;

/* "chase" effect config */
const int Const::CHASE_ONE_R = 255;
const int Const::CHASE_ONE_G = 0;
const int Const::CHASE_ONE_B = 0;

const int Const::CHASE_TWO_R = 0;
const int Const::CHASE_TWO_G = 255;
const int Const::CHASE_TWO_B = 0;

const int Const::CHASE_CLUSTER_SIZE = 2;
const int Const::CHASE_CLUSTER_DISTANCE = Const::CHASE_CLUSTER_SIZE * 2;

/* Config file config */
const String Const::CONFIG_FILE_LOCATION = "/config.json";

/* CONFIG-KEY device signature*/
const String Const::CONFIG_DEVICE_NAME_KEY = "deviceName";

/* CONFIG-KEY WiFi config*/
const String Const::CONFIG_WIFI_SSID_KEY = "wifiSSID";
const String Const::CONFIG_WIFI_PWD_KEY = "wifiPWD";

/* CONFIG-KEY updater config */
const String Const::CONFIG_UPDATER_PATH_KEY = "updaterPath";
const String Const::CONFIG_UPDATER_USERNAME_KEY = "updaterUsername";
const String Const::CONFIG_UPDATER_PASSWORD_KEY = "updaterPassword";

/* CONFIG-KEY MQTT config */
const String Const::CONFIG_SERVER_IP_KEY = "mqttServerIp";
const String Const::CONFIG_SERVER_PORT_KEY = "mqttServerPort";

/* CONFIG-KEY mode config*/
const String Const::CONFIG_SUSPEND_KEY = "suspend";
const String Const::CONFIG_CURRENT_MODE_KEY = "mode";
const String Const::CONFIG_GLOBAL_BRIGHTNESS_KEY = "globalBrightness";

/* CONFIG-KEY effect change config*/
const String Const::CONFIG_CHANGE_RESOLUTION_KEY = "changeResolution";
const String Const::CONFIG_CHANGE_DELAY_KEY = "changeDelay";

/* CONFIG-KEY effect speed config */
const String Const::CONFIG_RAINBOW_SPEED_KEY = "rainbowSpeed";
const String Const::CONFIG_FADE_SPEED_KEY = "fadeSpeed";
const String Const::CONFIG_CHASE_SPEED_KEY = "chaseSpeed";

/* CONFIG-KEY "color" effect config */
const String Const::CONFIG_COLOR_R_KEY = "colorR";
const String Const::CONFIG_COLOR_G_KEY = "colorG";
const String Const::CONFIG_COLOR_B_KEY = "colorB";

/* CONIFG-KEY "chase" effect config */
const String Const::CONFIG_CHASE_ONE_COLOR_R_KEY = "chaseOneR";
const String Const::CONFIG_CHASE_ONE_COLOR_G_KEY = "chaseOneG";
const String Const::CONFIG_CHASE_ONE_COLOR_B_KEY = "chaseOneB";

const String Const::CONFIG_CHASE_TWO_COLOR_R_KEY = "chaseTwoR";
const String Const::CONFIG_CHASE_TWO_COLOR_G_KEY = "chaseTwoG";
const String Const::CONFIG_CHASE_TWO_COLOR_B_KEY = "chaseTwoB";

const String Const::CONFIG_CHASE_CLUSTER_SIZE_KEY = "chaseClusterSize";
const String Const::CONFIG_CHASE_CLUSTER_DISTANCE_KEY = "chaseClusterDistance";

/* CONFIG-KEY strip config*/
const String Const::CONFIG_STRIP_PIN_KEY = "stripDin";
const String Const::CONFIG_STRIP_AMOUNT_KEY = "stripAmount";

/* CONFIG-KEY sensor config*/
const String Const::CONFIG_TEMPERATURE_PIN_KEY = "temperatureDin";
const String Const::CONFIG_MOTION_PIN_KEY = "motionDin";

/* CONFIG-KEY command config */
const String Const::COMMAND_FASTCMD = "fastcmd";

/* CONFIG-KEY mqtt topic config*/
const String Const::MQTT_ROOT_SUFFIX = "qbiot/";

const String Const::MQTT_STATE_GET_APPENDIX = "state/get";
const String Const::MQTT_STATE_POST_APPENDIX = "state";

const String Const::MQTT_SENSORS_GET_APPENDIX = "sensors/get";
const String Const::MQTT_SENSORS_POST_APPENDIX = "sensors/";

const String Const::MQTT_COMMAND_GET_APPENDIX = "command/get";
const String Const::MQTT_COMMAND_POST_APPENDIX = "command";
const String Const::MQTT_COMMAND_EXECUTE_APPENDIX = "command/execute";

const String Const::MQTT_CONFIG_GET_APPENDIX = "config/get";
const String Const::MQTT_CONFIG_SET_APPENDIX = "config/set";

const String Const::MQTT_EXTEND_ROOT_APPENDIX = "extend/#";

const String Const::MOTION_SENSOR_APPENDIX = "motion";
const String Const::TEMPERATURE_SENSOR_APPENDIX = "temperature";
