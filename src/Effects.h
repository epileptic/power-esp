#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

class Effects
{
private:
    Adafruit_NeoPixel *strip;

    long fadeHue = 0;
    long rainbowHue = 0;
    bool chaseMirror = true;

    void chaseFill(int chaseOne[3], int chaseTwo[3], int clusterSize, int clusterDistance);

public:
    Effects(Adafruit_NeoPixel *strip);
    void change(int oldColor[3], int newColor[3], float steps, int wait);
    void rainbow(int wait);
    void fade(int wait);
    void chase(int wait, int chaseOne[3], int chaseTwo[3], int clusterSize, int clusterDistance);

    void fadeOff(int wait, float steps);
};